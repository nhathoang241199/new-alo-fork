export const mapProvider: { [k: string]: string } = {
  pragmatic_play: 'PP',
  sbo: 'SBO',
  lottery: 'Lottery',
  we: 'WE',
  joker_gaming: 'Joker',
  sexy: 'Sexy',
};
