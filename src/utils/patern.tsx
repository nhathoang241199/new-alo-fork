export const ipWhiteListRegex =
  /^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})){3}$/;
export const nameRegex =
  /^[\w'\-,.][^0-9_!¡?÷?¿/\\+=@#$%^&*(){}|~<>;:[\]]{2,}\S$/;
export const userNameRegex = /^[a-zA-Z0-9_]{3,16}$/;
export const masterCoinRegex = /^[0-9]*(\.[0-9]*)?$/;
export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const smartOTPPinRegex = /^\d{6}$/;
export const numberRegex = /^[0-9]+$/;
// export const passwordRegex = /^(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,}$/;
export const passwordRegex =
  /^(?=.*[\d])(?=.*[a-zA-Z])[a-zA-Z\d!@#$%^&*()_+*+]{8,}$/;

export const regexTelegram =
  /(?:https?:\/\/)?(?:www\.)?(?:t\.me|telegram\.me)\/(?:joinchat\/)?([^/?]+)/;

export const regexWhatApp =
  /(?:https?:\/\/)?(?:www\.)?(?:chat\.whatsapp\.com|whatsapp\.com)\/(?:invite\/)?([^/?]+)/;

export const regexWeChat = /https:\/\/weixin\.qq\.com\/g\/([^/]+)/;

export const regexLine =
  /(?:https?:\/\/)?(?:www\.)?(?:line\.me\/(?:R\/)?(?:ti\/g\/)?)([^/?]+)/;
