import Color from 'color';

export const formatDateUTC = (date: string) => {
  const specificTimeZone = 'Asia/Jakarta';
  const options = { timeZone: specificTimeZone };
  const formattedDate = new Date(date).toLocaleString('en-US', options);

  return formattedDate;
};

export const generateColor = (centerColor: string) => {
  const color = centerColor;

  const scale = {
    100: Color(color).lighten(0.6).hex(),
    200: Color(color).lighten(0.45).hex(),
    300: Color(color).lighten(0.3).hex(),
    400: Color(color).lighten(0.15).hex(),
    500: Color(color).hex(),
    600: Color(color).darken(0.15).hex(),
    700: Color(color).darken(0.3).hex(),
    800: Color(color).darken(0.45).hex(),
    900: Color(color).darken(0.6).hex(),
    950: Color(color).darken(0.7).hex(),
  };

  return scale;
};
