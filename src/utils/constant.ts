import { HiMiniPresentationChartLine } from 'react-icons/hi2';
import { IoWallet } from 'react-icons/io5';
import { RiSettings5Fill } from 'react-icons/ri';
import { TfiCup } from 'react-icons/tfi';

export enum ECurrency {
  vnd = 'VND',
  idr = 'IDR',
}

export const menuData = [
  {
    icon: HiMiniPresentationChartLine,
    text: 'Dashboard',

    href: '/profile',
  },
  {
    icon: TfiCup,
    text: 'VIP Progress',

    href: '/profile/level',
  },
  {
    icon: IoWallet,
    text: 'Transactions',
    href: '',
    menu: {
      isOpen: false,
      data: [
        {
          text: 'Deposit',
          href: '/profile/transactions/deposit',
        },
        {
          text: 'Withdraw',
          href: '/profile/transactions/withdraw',
        },
        {
          text: 'Bets',
          href: '/profile/transactions/bets',
        },
        {
          text: 'Other',
          href: '/profile/transactions/other',
        },
      ],
    },
  },
  {
    icon: RiSettings5Fill,
    text: 'Setting',
    href: '',
    menu: {
      isOpen: false,
      data: [
        {
          text: 'Profile',
          href: '/profile/setting',
        },
        {
          text: 'Security',
          href: '/profile/setting/security',
        },
      ],
    },
  },
];
