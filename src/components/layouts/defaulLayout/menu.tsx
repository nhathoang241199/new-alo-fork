import { Flex, Text } from '@chakra-ui/react';
import { IoIosMenu } from 'react-icons/io';
import { TbHome } from 'react-icons/tb';
import { IoGiftOutline } from 'react-icons/io5';
import { FiLogIn } from 'react-icons/fi';
import Link from 'next/link';

const menuItems = [
  {
    isLink: false,
    title: 'Menu',
    src: <IoIosMenu size={24} />,
    href: '',
  },
  { isLink: true, title: 'Home', src: <TbHome size={24} />, href: '/' },
  {
    isLink: true,
    title: 'Bonus',
    src: <IoGiftOutline size={24} />,
    href: '',
  },
  { isLink: false, title: 'Log in', src: <FiLogIn size={24} />, href: '' },
];

type TProps = {
  isOpenSidebar: boolean;
  // eslint-disable-next-line no-unused-vars
  setIsOpenSideBar: (value: boolean) => void;
};

const Menu: React.FC<TProps> = ({ isOpenSidebar, setIsOpenSideBar }) => {
  const handleChangeOpen = (isLink: boolean) => {
    if (isLink) {
      return;
    }
    setIsOpenSideBar(!isOpenSidebar);
  };
  return (
    <Flex
      h="64px"
      bottom={0}
      bg="rgb(21, 27, 37)"
      left={0}
      position="fixed"
      w="100vw"
      padding="0 8px"
      zIndex="5"
      boxSizing="border-box"
      display={{ base: 'flex', md: 'none' }}
      alignItems="center"
    >
      {menuItems.map((item, index) => {
        return (
          <Link
            key={index}
            href={item.href}
            onClick={() => handleChangeOpen(item.isLink)}
            style={{
              width: '100%',
              padding: '8px 0px 16px',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
              placeItems: 'center',
              cursor: 'pointer',
            }}
          >
            <Flex flexDirection="column" alignItems="center" textAlign="center">
              <Flex textColor="white" w="24px" h="27px" alignItems="center">
                {item.src}
              </Flex>
              <Text
                marginTop="3px"
                fontSize="10px"
                fontWeight="500"
                textColor="rgb(173, 173, 173)"
              >
                {item.title}
              </Text>
            </Flex>
          </Link>
        );
      })}
    </Flex>
  );
};

export default Menu;
